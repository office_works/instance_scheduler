variable "role_name" {
  description = "Name of the IAM role"
  type        = string
}

variable "tag_key" {
  description = "Tag key for the role"
  type        = string
}