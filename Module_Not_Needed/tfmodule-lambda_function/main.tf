resource "aws_lambda_function" "lambda" {
  filename      = var.filename
  function_name = var.function_name
  role          = var.role_arn
  handler       = var.handler

  source_code_hash = filebase64sha256(var.filename)

  runtime = var.runtime
  timeout = var.timeout
}