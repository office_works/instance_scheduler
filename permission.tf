resource "aws_lambda_permission" "lambda_permission_start" {
  statement_id  = "AllowExecutionFromCloudWatch"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.lambda-start.arn
  principal     = "events.amazonaws.com"
  source_arn    = aws_cloudwatch_event_rule.cloudwatch_event_rule-start.arn
}

resource "aws_lambda_permission" "lambda_permission_start_weekend" {
  statement_id  = "AllowExecutionFromCloudWatchTwo"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.lambda-start.arn
  principal     = "events.amazonaws.com"
  source_arn    = aws_cloudwatch_event_rule.cloudwatch_event_rule-start-weekend.arn
}
resource "aws_lambda_permission" "lambda_permission_stop" {
  statement_id  = "AllowExecutionFromCloudWatch"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.lambda-stop.arn
  principal     = "events.amazonaws.com"
  source_arn    = aws_cloudwatch_event_rule.cloudwatch_event_rule-stop.arn
}