resource "aws_cloudwatch_event_target" "lambda_stop_func" {
  target_id = var.target_id
  rule      = var.rule_name
  arn       = var.lambda_function_arn
}