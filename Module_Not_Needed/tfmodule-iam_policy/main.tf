resource "aws_iam_policy" "lambda-policy" {
  name   = var.policy_name
  policy = var.policy_document
}