output "policy_arn" {
  value = aws_iam_policy.lambda-policy.arn
}

output "policy_name" {
  value = aws_iam_policy.lambda-policy.name
}