import boto3

def lambda_handler(event, context):
    ec2 = boto3.resource("ec2")

    regions = []
    for region in ec2.meta.client.describe_regions()['Regions']:
        regions.append(region['RegionName'])

    for region in regions:
        ec2 = boto3.resource("ec2", region_name=region)

        print("EC2 region is: ", region)

        # Modify the filter to fetch instances with specific tag key-value pair
        instances_with_tag = ec2.instances.filter(Filters=[
            {'Name': 'tag:Runschedule', 'Values': ['Weekdays']},
            {'Name': 'instance-state-name', 'Values': ['running']}
        ])

        for instance in instances_with_tag:
            instance.stop()
            print("The following EC2 instances are now in the stopped state:", instance.id)
