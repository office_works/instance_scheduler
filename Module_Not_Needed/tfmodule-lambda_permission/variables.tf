variable "statement_id" {
  description = "A unique identifier for the Lambda permission statement."
  type        = string
}

variable "action" {
  description = "The action that CloudWatch is allowed to perform on the Lambda function."
  type        = string
}

variable "lambda_function_name" {
  description = "Name of the Lambda function to allow CloudWatch to invoke."
  type        = string
}

variable "principal" {
  description = "The AWS service principal that can invoke the Lambda function."
  type        = string
}

variable "cloudwatch_event_rule_arn" {
  description = "ARN of the CloudWatch Event Rule that triggers the Lambda function."
  type        = string
}