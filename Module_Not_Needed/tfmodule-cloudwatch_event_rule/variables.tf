variable "name" {
  description = "Name of the CloudWatch Event Rule."
  type        = string
}

variable "description" {
  description = "Description of the CloudWatch Event Rule."
  type        = string
}

variable "schedule_expression" {
  description = "The schedule expression for the CloudWatch Event Rule."
  type        = string
}