import boto3

def lambda_handler(event, context):
    # Restrict Lambda function to operate only in the 'ap-south-1' region
    target_region = 'ap-south-1'
    if context.invoked_function_arn.split(':')[3] != target_region:
        print(f"Lambda function is invoked in a region other than {target_region}. Exiting.")
        return

    ec2 = boto3.resource("ec2", region_name=target_region)

    print("EC2 region is: ", target_region)

        # Fetch all instances with a 'stopchedule' tag
    instances_with_stopschedule_tag = ec2.instances.filter(Filters=[
            {'Name': 'tag-key', 'Values': ['Stopschedule']}
        ])

    for instance in instances_with_stopschedule_tag:
            # Get the 'stopschedule' tag value for the current instance
            tags = {tag['Key']: tag['Value'] for tag in instance.tags}
            stopschedule_tag_value = tags.get('Stopschedule')

            if stopschedule_tag_value:
                print(f"Found instance with Runschedule tag value: {stopschedule_tag_value}")

                # Check if the event rule matches the instance's stopschedule tag value
                event_rule_name = event['resources'][0].split('/')[-1]
                if event_rule_name == f'ec2-stop-{stopschedule_tag_value}':
                    # Stop the instance if it's in a 'running' state
                    if instance.state['Name'] == 'running':
                        instance.stop()
                        print(f"Stopped instance with ID: {instance.id}")
                    else:
                        print(f"Instance with ID {instance.id} is not in 'running' state, skipping stop operation.")
                else:
                    print(f"Event rule name '{event_rule_name}' does not match Runschedule tag value '{stopschedule_tag_value}', skipping.")
            else:
                print(f"Instance with ID {instance.id} does not have a stopschedule tag, skipping.")    