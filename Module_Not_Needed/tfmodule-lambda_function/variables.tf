variable "filename" {
  description = "The path to the ZIP file containing the Lambda function code."
  type        = string
}

variable "function_name" {
  description = "The name of the Lambda function."
  type        = string
}

variable "role_arn" {
  description = "The ARN of the IAM role for the Lambda function."
  type        = string
}

variable "handler" {
  description = "The entry point for the Lambda function."
  type        = string
}

variable "runtime" {
  description = "The runtime environment for the Lambda function."
  type        = string
}

variable "timeout" {
  description = "The amount of time the Lambda function has to run in seconds."
  type        = number
}