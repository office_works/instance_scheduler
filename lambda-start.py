import boto3

def lambda_handler(event, context):
    # Restrict Lambda function to operate only in the 'ap-south-1' region
    target_region = 'ap-south-1'
    if context.invoked_function_arn.split(':')[3] != target_region:
        print(f"Lambda function is invoked in a region other than {target_region}. Exiting.")
        return

    ec2 = boto3.resource("ec2", region_name=target_region)

    print("EC2 region is: ", target_region)

        # Fetch all instances with a 'startschedule' tag
    instances_with_startschedule_tag = ec2.instances.filter(Filters=[
            {'Name': 'tag-key', 'Values': ['Startschedule']}
        ])

    for instance in instances_with_startschedule_tag:
            # Get the 'startschedule' tag value for the current instance
            tags = {tag['Key']: tag['Value'] for tag in instance.tags}
            startschedule_tag_value = tags.get('Startschedule')

            if startschedule_tag_value:
                print(f"Found instance with Runschedule tag value: {startschedule_tag_value}")

                # Check if the event rule matches the instance's startschedule tag value
                event_rule_name = event['resources'][0].split('/')[-1]
                if event_rule_name == f'ec2-start-{startschedule_tag_value}':
                    # Start the instance if it's in a 'stopped' state
                    if instance.state['Name'] == 'stopped':
                        instance.start()
                        print(f"Started instance with ID: {instance.id}")
                    else:
                        print(f"Instance with ID {instance.id} is not in 'stopped' state, skipping start operation.")
                else:
                    print(f"Event rule name '{event_rule_name}' does not match Runschedule tag value '{startschedule_tag_value}', skipping.")
            else:
                print(f"Instance with ID {instance.id} does not have a startschedule tag, skipping.")    