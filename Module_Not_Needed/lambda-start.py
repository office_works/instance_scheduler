import boto3

def lambda_handler(event, context):
    ec2 = boto3.resource("ec2")

    regions = []
    for region in ec2.meta.client.describe_regions()['Regions']:
        regions.append(region['RegionName'])

    for region in regions:
        ec2 = boto3.resource("ec2", region_name=region)

        print("EC2 region is: ", region)

        event_rule_name = event['resources'][0].split('/')[-1]
        if event_rule_name == 'ec2-start-rule':  # Assuming Event Rule 1 runs at 01:30 AM
            tag_value = 'Weekdays'
        elif event_rule_name == 'ec2-start-weekend':  # Assuming Event Rule 2 runs at 02:30 AM
            tag_value = 'Weekends'
        else:
            print(f"Unknown event rule: {event_rule_name}")
            return
        # Modify the filter to fetch instances with specific tag key-value pair
        instances_with_tag = ec2.instances.filter(Filters=[
            {'Name': 'tag:Runschedule', 'Values': [tag_value]},
            {'Name': 'instance-state-name', 'Values': ['stopped']}
        ])

        for instance in instances_with_tag:
            instance.start()
            print("The following EC2 instances are now in the started state:", instance.id)
