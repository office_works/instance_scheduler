output "role_arn" {
  value = aws_iam_role.lambda-role.arn
}

output "role_name" {
  value = aws_iam_role.lambda-role.name
}