resource "aws_cloudwatch_event_rule" "cloudwatch_event_rule-stop" {
  name                = "ec2-stop-testdays"
  description         = "Trigger EC2 stop instance"
  schedule_expression = "cron(05 10 ? * TUE *)"

  tags = {
    Runschedule = "ec2-stop-testdays"
  }
}

resource "aws_cloudwatch_event_rule" "cloudwatch_event_rule-start" {
  name                = "ec2-start-testdays"
  description         = "Trigger EC2 start instance"
  schedule_expression = "cron(48 08 ? * TUE *)"

  tags = {
    Runschedule = "ec2-start-testdays"
  }
}
resource "aws_cloudwatch_event_rule" "cloudwatch_event_rule-start-weekend" {
  name                = "ec2-start-testtwodays"
  description         = "Trigger EC2 start instance"
  schedule_expression = "cron(53 08 ? * TUE *)"

  tags = {
    Runschedule = "ec2-start-testtwodays"
  }
}
resource "aws_cloudwatch_event_target" "lambda_start_func" {
  target_id = "lambda-start"
  rule      = aws_cloudwatch_event_rule.cloudwatch_event_rule-start.name
  arn       = aws_lambda_function.lambda-start.arn
}
resource "aws_cloudwatch_event_target" "lambda_start_weekend_func" {
  target_id = "lambda-start"
  rule      = aws_cloudwatch_event_rule.cloudwatch_event_rule-start-weekend.name
  arn       = aws_lambda_function.lambda-start.arn
}

resource "aws_cloudwatch_event_target" "lambda_stop_func" {
  target_id = "lambda-stop"
  rule      = aws_cloudwatch_event_rule.cloudwatch_event_rule-stop.name
  arn       = aws_lambda_function.lambda-stop.arn
}