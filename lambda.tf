resource "aws_lambda_function" "lambda-stop" {
  filename      = "lambda-stop.zip"
  function_name = "lambda-stop"
  role          = aws_iam_role.lambda-role.arn
  handler       = "lambda-stop.lambda_handler"

  source_code_hash = filebase64sha256("lambda-stop.zip")

  runtime = "python3.7"
  timeout = 63
}

resource "aws_lambda_function" "lambda-start" {
  filename      = "lambda-start.zip"
  function_name = "lambda-start"
  role          = aws_iam_role.lambda-role.arn
  handler       = "lambda-start.lambda_handler"

  source_code_hash = filebase64sha256("lambda-start.zip")

  runtime = "python3.7"
  timeout = 63
}