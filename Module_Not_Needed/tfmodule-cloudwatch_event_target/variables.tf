variable "target_id" {
  description = "A unique identifier for the CloudWatch Event Target."
  type        = string
}

variable "rule_name" {
  description = "Name of the CloudWatch Event Rule to associate with the target."
  type        = string
}

variable "lambda_function_arn" {
  description = "ARN of the Lambda function to be invoked by the CloudWatch Event Target."
  type        = string
}