variable "policy_name" {
  description = "Name of the IAM policy"
  type        = string
  default     = "lambda-ec2-stop-start-new"
}

variable "policy_document" {
  description = "The IAM policy document in JSON format"
  type        = string
}